var outdoors = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 16,
    attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    id: 'mapbox/outdoors-v11', 
    tileSize: 512,
    zoomOffset: -1
})

var satellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    maxZoom: 19,
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});

var baseLayers = {
    "outdoors": outdoors,
    "satellite": satellite,
};

var mymap = L.map('map',{
    fullscreenControl: true,
    center: [44.966057, 5.567906],
    zoom: 11,
    layers: [outdoors]
  });

// var gpx = 'GPSTracks/4214306-track-1598127993-634.gpx'; // URL to your GPX file or the GPX itself
// new L.GPX(gpx, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}}).on('loaded', function(e) {
//   mymap.fitBounds(e.target.getBounds());
// }).addTo(mymap);

// var gpx = 'GPSTracks/5404882-track-1598128005-70.gpx'; // URL to your GPX file or the GPX itself
// new L.GPX(gpx, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}}).on('loaded', function(e) {
//   mymap.fitBounds(e.target.getBounds());
// }).addTo(mymap);

// var gpx = "GPSTracks/Sud_Vercors.gpx"; // URL to your GPX file or the GPX itself
// new L.GPX(gpx, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}}).on('loaded', function(e) {
//   mymap.fitBounds(e.target.getBounds());
// }).addTo(mymap);

// var gpx = "GPSTracks/Grenoble_Pre-Peyret.gpx"
// new L.GPX(gpx, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'red', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
//   mymap.fitBounds(e.target.getBounds());
// }).addTo(mymap);

// var gpx = "GPSTracks/Pre-Peyret_--_Chatillon_(Archiane).gpx"
// new L.GPX(gpx, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'red', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
//   mymap.fitBounds(e.target.getBounds());
// }).addTo(mymap);

function display_gpx(map,listOfFilesPath,Path,color) {
  for (var i = 0; i < listOfFilesPath.length; i += 1) {
  new L.GPX(listOfFilesPath[i], {
    async: true,
    marker_options: {
    startIconUrl: '',
    endIconUrl: '',
    shadowUrl: ''
    },
    polyline_options: {
      color: color,
      opacity: 0.75,
      weight: 3,
      lineCap: 'round'},
    async: true }
    ).on('loaded', function(e) {
      map.fitBounds(e.target.getBounds());
    }).addTo(Path);
  }
}

var RTTS_2021 = 'GPSTracks_England/RTTS_2021.gpx'; // URL to your GPX file or the GPX itself
var Circuit1 = L.layerGroup();
new L.GPX(RTTS_2021, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'red', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
   mymap.fitBounds(e.target.getBounds());
}).addTo(Circuit1);

var DidcotRidgeway = 'GPSTracks_England/2020_11_22_TimeLess.gpx' ; // URL to your GPX file or the GPX itself
var Circuit2 = L.layerGroup();
new L.GPX(DidcotRidgeway, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'blue', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
   mymap.fitBounds(e.target.getBounds());
}).addTo(Circuit2);

var The_Ridgeway = 'GPSTracks_England/The_Ridgeway.gpx' ; // URL to your GPX file or the GPX itself
var Circuit3 = L.layerGroup();
new L.GPX(The_Ridgeway, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'blue', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
   mymap.fitBounds(e.target.getBounds());
}).addTo(Circuit3);

var Thames_Path = 'GPSTracks_England/Thames_Path.gpx' ; // URL to your GPX file or the GPX itself
var Circuit4 = L.layerGroup();
new L.GPX(Thames_Path, {async: true, marker_options: {startIconUrl: '', endIconUrl: '', shadowUrl: ''}, polyline_options: {color: 'green', opacity: 0.75, weight: 3, lineCap: 'round'}}).on('loaded', function(e) {
   mymap.fitBounds(e.target.getBounds());
}).addTo(Circuit4);

var Wysis_Way = 'GPSTracks_England/Wysis_Way.gpx' ; // URL to your GPX file or the GPX itself
var Circuit5 = L.layerGroup();
new L.GPX(Wysis_Way, {
            async: true, 
            marker_options: {
              startIconUrl: '', 
              endIconUrl: '', 
              shadowUrl: ''
            },
          polyline_options: {
            color: 'green',
            opacity: 0.75,
            weight: 3,
            lineCap: 'round'
            },
            async: true
          }
        ).on('loaded', function(e) {
          mymap.fitBounds(e.target.getBounds());
        }
      ).addTo(Circuit5);


var overlays = {
  "RTTS 2021": Circuit1,
  "Didcot Ridgeway": Circuit2,
  "Ridgeway": Circuit3,
  "Thames path": Circuit4,
  "Wysis": Circuit5
};

L.control.scale().addTo(mymap);
L.control.layers(baseLayers, overlays).addTo(mymap);